mixKernel
================

Kernel-based methods are powerful methods for integrating heterogeneous
types of data. mixKernel aims at providing methods to combine kernel for
unsupervised exploratory analysis. Different solutions are provided to
compute a meta-kernel, in a consensus way or in a way that best
preserves the original topology of the data. mixKernel also integrates
kernel PCA to visualize similarities between samples in a non linear
space and from the multiple source point of view. Functions to select
and display important variables are also provided in the package in an
unsupervised and kernel association frameworks.

Installation instructions are provided below.

## Installation of python dependencies

The following python modules are required for the functions performing
feature selection in `mixKernel`: autograd, scipy, sklearn, numpy

``` python
pip3 install autograd
pip3 install scipy
pip3 install sklearn
pip3 install numpy
```

## Installation of Bioconductor dependencies

Two Bioconductor packages are required for `mixKernel` installation:
`mixOmics` and `phyloseq`:

``` r
install.packages("BiocManager")
BiocManager::install("mixOmics")
BiocManager::install("phyloseq")
```

## mixKernel installation

Finally the installation is completed with:

``` r
install.packages("mixKernel")
```

## References

Mariette, J. and Villa-Vialaneix, N. (2018). Unsupervised multiple
kernel learning for heterogeneous data integration. *Bioinformatics*,
**34**(6), 1009-1015.

Brouard, C., Mariette, J., Flamary, R., & Vialaneix, N. (2022). Feature
selection for kernel methods in systems biology. *NAR Genomics and
Bioinformatics*, **4**(1), lqac014.
